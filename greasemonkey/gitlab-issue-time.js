// ==UserScript==
// @name                Gitlab easy-time-track
// @description         
// @description:zh-CN   Github/Gitlab

// @author              GallenHu
// @namespace           https://hgl2.com
// @license             MIT
// @icon                https://github.githubassets.com/favicons/favicon.png

// @grant               none
// @run-at              document-end
// @include             *://github.com/*
// @include             *://gitlab.com/*

// @date                03/22/2021
// @modified            03/22/2021
// @version             0.1.0
// @require             https://cdn.staticfile.org/jquery/1.12.2/jquery.min.js
// ==/UserScript==


(function () {
    'use strict';

    const host = window.location.host;
    const pathname = window.location.pathname;
    const isGithub = host.endsWith('github.com');
    const isGitlab = host.endsWith('gitlab.com');

    if (isGithub) {
        const userName = document.querySelector('meta[name="user-login"]').content;

        const nav = document.querySelector('nav.d-flex');
        const html = `<a class="js-selected-navigation-item Header-link flex-auto mt-md-n3 mb-md-n3 py-2 py-md-3 mr-0 mr-md-3 border-top border-md-top-0 border-white-fade-15" data-ga-click="Header, click, Nav menu - item:marketplace context:user" data-octo-click="marketplace_click" data-octo-dimensions="location:nav_bar" data-selected-links="/${userName}?tab=repositories" href="/${userName}?tab=repositories">MyRepos-仓库列表</a>
        <a class="js-selected-navigation-item Header-link flex-auto mt-md-n3 mb-md-n3 py-2 py-md-3 mr-0 mr-md-3 border-top border-md-top-0 border-white-fade-15" data-ga-click="Header, click, Nav menu - item:marketplace context:user" data-octo-click="marketplace_click" data-octo-dimensions="location:nav_bar" data-selected-links="/${userName}?tab=repositories" href="/${userName}?tab=stars">MyStars-收藏列表</a>
        `
        $(nav).append(html);
    }

    if (isGitlab) {
        const userName = window.gon.current_username;
        const nav = document.querySelector('ul.navbar-sub-nav');
        const html = `<li class="dropdown"><a href="/users/${userName}/projects">MyRepos</a></li>
        <li class="dropdown"><a href="/users/${userName}/starred">MyStars</a></li>
        `
        $(nav).append(html);
    }
})();

