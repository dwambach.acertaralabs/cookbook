import unittest
from atlas_tools import atlas_ppf_to_2_0_0
#May need to modify PYTHONPATH:
## C:\>set PYTHONPATH=%PYTHONPATH%;C:\path\to\lib

class Test_atlas_ppf_to_2_0_0(unittest.TestCase):

    def test_convert_line(self):
        test_str = 'ReportTemplate_BothTests\t=\tReportTemplate_BothTests_Standard.html'
        pass_str = 'ReportTemplate_BothTests\t=\tUser\\ReportTemplate_BothTests_Default.html'        
        result_str = atlas_ppf_to_2_0_0.convert_line(test_str)
        self.assertEqual(result_str, pass_str, 'Expected {}\nReceived: {}'.format(
                         pass_str, result_str))

        test_str = 'ReportTemplate_BothTests\t=\tUser\\ReportTemplate_BothTests_Default.html'
        pass_str = 'ReportTemplate_BothTests\t=\tUser\\ReportTemplate_BothTests_Default.html'        
        result_str = atlas_ppf_to_2_0_0.convert_line(test_str)
        self.assertEqual(result_str, pass_str, 'Expected {}\nReceived: {}'.format(
                         pass_str, result_str))

    def test_convert_file(self):
        self.assertEqual(sum([1,2,3]), 6, 'Should be 6')

# def test_name_sanitizatoin(self):
#         pass_str = 'Model   =   TEST--------123 //Model designation for array or probe being tested'        
#         test_str = r'Model   =   TEST:|?<>\/*123 //Model designation for array or probe being tested'
#         result_str = atlas_ppf_to_2_0_0.convert_line(test_str)
#         self.assertEqual(result_str, pass_str, 'Expected {}\nReceived: {}'.format(
#                          pass_str, result_str))


if __name__ == '__main__':
    unittest.main()