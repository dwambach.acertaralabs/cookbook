#!/usr/bin/env python3
"""Modifies PPF files
Looks to change the following section:
 ReportTemplate_BothTests    =    User\\ReportTemplate_BothTests_Acertara_PerElementZ_Header.html
"""
import utils.argparse_utils as argparse_utils  # pylint: disable-msg=E0401

PROGRAM_DESC = 'Converts older ATLAS parameter files (PPFs) ReportTemplate_BothTests'


def replace_reporttemplate_bothtests(file_contents):
    """Replaces acceptance script value
    """
    # new_l = os.linesep
    desired_param = 'ReportTemplate_BothTests'
    desired_value = 'User\\ReportTemplate_BothTests_Acertara_PerElementZ_Header.html'
    found_param = False
    for idx, line in enumerate(file_contents):
        if line.startswith(desired_param):
            print('[DEBUG] found param')
            file_contents[idx] = '{}\t=\t{}{}'.format(desired_param, desired_value, '\n')
            found_param = True
    if not found_param:
        print('[DEBUG] Value not found. Adding it')
    return file_contents


def run_task_on_file(filep):
    """Will perform tasks on filep
    NOTE: this function is sort of a generic placeholder
    where a different script can be called
    """
    # file_contents = file_utils.get_stripped_file_contents(filep)
    with open(filep, 'r') as freader:
        file_contents = freader.readlines()

    new_contents = replace_reporttemplate_bothtests(file_contents)

    with open(filep, 'w') as fwriter:
        fwriter.writelines(new_contents)


if __name__ == '__main__':
    # pylint: disable-msg=C0103
    workon_files = argparse_utils.argparse_workon_files(PROGRAM_DESC)

    if len(workon_files) > 0:
        print('[*] Number of files to read: {}'.format(len(workon_files)))
        err_occured = False
        for workon_file in workon_files:
            try:
                run_task_on_file(workon_file)
            except OSError as err:
                err_occured = True
        if err_occured:
            print('[!] Finished with errors.')
        else:
            print('[*] Successfully read {} file(s)'.format(len(workon_files)))
    else:
        print('[!] No files read.')
