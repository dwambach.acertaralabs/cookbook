#!/usr/bin/env python3
r"""Modifies PPF files for ATLAS v2.0.0 support

ATLAS v2.0.0 PPF changes:

ReportTemplate settings changed, adding User\ dir, and changing _Standard.html to _Default.html
Prior to ALAS v2.0.0:
    ReportTemplate_PulseTest    =   ReportTemplate_PulseTest_Standard.html

ATLAS v2.0.0 and higher require the following format:
    ReportTemplate_PulseTest    =   User\ReportTemplate_PulseTest_Default.html
"""
import argparse
import os

PROGRAM_DESC = 'Converts older ATLAS parameter files (PPFs) to be v2.0.0 compatible'

PARAM_STR = 'ReportTemplate_'  # The PPF params to look for
INSERT_STR = 'User\\'  # New string val to be inserted into old
INSERT_AFTER_STR = '\t=\t'  # Insert INSERT_STR after this sub_str
SEARCH_REPLACE_STRS = ('Standard', 'Default')  # Search for first str, replace with the second str


def convert_line(old_str):
    """given old_str, perform necessary changes before returning new_str"""
    # test if line has already been modified
    # (ie. we dont want to insert INSERT_STR again)
    if old_str.find(INSERT_STR) != (-1):
        new_str = old_str
    else:
        insert_idx = old_str.find(INSERT_AFTER_STR) + len(INSERT_AFTER_STR)
        new_str = old_str[:insert_idx] + INSERT_STR + old_str[insert_idx:]
        new_str = new_str.replace(SEARCH_REPLACE_STRS[0], SEARCH_REPLACE_STRS[1])
    return new_str


def convert_file(filep):
    """Given file path, filep, get modifications and rewrite file"""
    file_lines = []
    with open(filep, 'r') as in_file:
        file_lines = in_file.readlines()

    with open(filep, 'w') as out_file:
        for str_line in file_lines:
            if str_line.startswith(PARAM_STR):
                str_line = convert_line(str_line)
            out_file.write(str_line)


if __name__ == '__main__':
    # pylint: disable-msg=C0103
    argparser = argparse.ArgumentParser(description=PROGRAM_DESC)
    argparser.add_argument('-v', dest='verbose', action='store_true', help='Turn verbosity on')
    argparser.add_argument('-r', dest='recursive',
                           help='Recursive traversing')
    argparser.add_argument('-f', dest='read_file', help='File containing list of files to search')
    argparser.add_argument('files', nargs='*', help='File(s) to search')
    args = argparser.parse_args()

    workon_files = set()  # set of files to be worked on
    if args.files:
        for arg_filep in args.files:
            workon_files.add(arg_filep.strip())
    if args.read_file:
        with open(args.read_file) as freader:
            for arg_filep in freader:
                workon_files.add(arg_filep.strip())
    if args.recursive:
        for (root, _, files) in os.walk(args.recursive):
            for f in files:
                arg_filep = os.path.join(root, f)
                workon_files.add(arg_filep.strip())

    if len(workon_files) < 1:
        argparser.print_help()
        print('[!] Zero files read in.')
    else:
        print('[*] Number of files to read: {}'.format(len(workon_files)))
        err_occured = False
        for workon_file in workon_files:
            try:
                convert_file(workon_file)
            except OSError as err:
                err_occured = True
                if args.verbose:
                    print('[!] file: {}\tERROR'.format(workon_file))
                    print(repr(err))
            else:
                if args.verbose:
                    print('[-] file: {}\tSUCCESS'.format(workon_file))
        if err_occured:
            print('[!] Finished with errors.')
        else:
            print('[*] Successfully read {} file(s)'.format(len(workon_files)))
