#!/usr/bin/env python3
"""atlas_ppf_file_name_sanitizer.py
Windows filenames can not contain the following characters: /\"*?|<>
Some Model names saved in PPFs contain some of these bad characters,
which then causes ATLAS to throw an error when the user saves a report

This script will recursively traverse a directory tree looking for PPF
files (.tab file extension, can be set with FILE_EXT) and replace any illegal chars
(can be set via ILLEGAL_CHARS) in the Model
name (can be set with PARAM_STR) with a '-' character (can be changed via REPLACEMENT_CHAR) variable
"""
import argparse
import os

PROGRAM_DESC = 'Sanitizes Model names, in ATLAS parameter files (PPFs), \
from containing illegal characters'

FILE_EXT = '.tab'  # File extension to look for
PARAM_STR = 'Model'  # The PPF params to look for
SANITIZE_INDEX = 2  # strs will be split(), sanitize the str at this index
ILLEGAL_CHARS = ':\\/<>*?|'  # Characters that can not be in a Windows filename
REPLACEMENT_CHAR = '-'  # Replace any and all ILLEGAL_CHARS with REPLACEMENT_CHAR


def make_str_translation(to_replace, replacement_char):
    """Makes a str translation object
    - to_replace a string of chars that will be replaced
    - replacement_char a char that will replace chars in to_replace
    Return a translation dict object which can be fed into str.translate
    """
    # in a translate object, the to_replace and replace_with strs need to be same length
    replace_with = replacement_char * len(to_replace)
    a_translator = str.maketrans(to_replace, replace_with)
    return a_translator


def sanitize_file(filep, translator, verbosity=False):
    """Given file path, filep, make modifications and rewrite file
        Returns True if file needed to be modified, False otherwise
    """
    with open(filep, 'r') as in_file:
        in_file_lines = in_file.readlines()

    with open(filep, 'w') as out_file:
        for in_file_line in in_file_lines:
            if in_file_line.startswith(PARAM_STR):
                split_line = in_file_line.split()
                str_to_sanitize = split_line[SANITIZE_INDEX]
                # Only need to run translate on this particular str object, not the whole line
                sanitized_str = str_to_sanitize.translate(translator)
                if str_to_sanitize != sanitized_str:
                    if verbosity:
                        print('[*] {} sanitzied to {}'.format(str_to_sanitize, sanitized_str))
                    in_file_line = in_file_line.replace(str_to_sanitize, sanitized_str)
            out_file.write(in_file_line)


if __name__ == '__main__':
    # pylint: disable-msg=C0103
    argparser = argparse.ArgumentParser(description=PROGRAM_DESC)
    argparser.add_argument('-v', dest='verbose', action='store_true', help='Turn verbosity on')
    argparser.add_argument('-r', dest='recursive',
                           help='Recursive traversing')
    argparser.add_argument('-f', dest='read_file', help='File containing list of files to search')
    argparser.add_argument('files', nargs='*', help='File(s) to search')
    args = argparser.parse_args()

    workon_files = set()  # set of files to be worked on
    if args.files:
        for arg_file in args.files:
            workon_files.add(arg_file.strip())
    if args.read_file:
        with open(args.read_file) as freader:
            for arg_file in freader:
                workon_files.add(arg_file.strip())
    if args.recursive:
        for (root, _, files) in os.walk(args.recursive):
            for f in files:
                arg_file = os.path.join(root, f)
                workon_files.add(arg_file.strip())

    if len(workon_files) < 1:
        argparser.print_help()
        print('[!] Zero files read in.')
    else:
        print('[*] Number of files to read: {}'.format(len(workon_files)))
        sanitized_translator = make_str_translation(ILLEGAL_CHARS, REPLACEMENT_CHAR)
        err_occured = False
        for workon_file in workon_files:
            try:
                sanitize_file(filep=workon_file,
                              translator=sanitized_translator,
                              verbosity=args.verbose)
            except OSError as err:
                err_occured = True
                if args.verbose:
                    print('[!] file: {}\tERROR'.format(workon_file))
                    print(repr(err))
            else:
                if args.verbose:
                    print('[-] file: {}\tSUCCESS'.format(workon_file))
        if err_occured:
            print('[!] Finished with errors.')
        else:
            print('[*] Successfully read {} file(s)'.format(len(workon_files)))
