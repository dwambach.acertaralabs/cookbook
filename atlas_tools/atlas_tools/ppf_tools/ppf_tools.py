#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
#ppf_tools.py
"""Driver for a collection of tasks used on Atlas PPF files
Available Classes:
- PPFile: Atlas PPF File interface
Available Functions:
- open_ppf: opens file as a PPFFile
"""

# Magic numbers for PPF Content
PPF_VALUE_INDEX = 2


def _read_file_contents(filepath):
    """Generic file reader, returns contents as list"""
    file_contents = []
    with open(filepath, 'r') as freader:
        file_contents = freader.readlines()
    return file_contents


def _write_file_contents(filepath, contents):
    """Generic, 'w' file writer
    Arguments:
    - filepath: file to write to
    - contents: list of strings to write to filepath
    """
    with open(filepath, 'w') as fwriter:
        fwriter.writelines(contents)


def _make_str_translation(to_replace, replacement_char):
    """Makes a str translation object
    - to_replace a string of chars that will be replaced
    - replacement_char a char that will replace chars in to_replace
    Return a translation dict object which can be fed into str.translate
    """
    # in a translate object, the to_replace and replace_with strs need to be same length
    replace_with = replacement_char * len(to_replace)
    a_translator = str.maketrans(to_replace, replace_with)
    return a_translator


class PPFFileError(Exception):
    """Represents an error handler for PPFFile
    """
    pass

class PPFFile:
    """Represents an interface to a Atlas PPF file
    Available attributes:
    - ppf_file: Filepath to a Atlas ppf file
    - overwrite: Boolean, set True Overwrite the old Atlas file,
        set False to create new file with suffix '.mod' for modified
    """
    def __init__(self, filep, overwrite=False):
        self.ppf_file = filep
        self.file_overwrite = overwrite
        self.ppf_contents = _read_file_contents(self.ppf_file)
    
    def set_parameter(self, param, value):
        """Sets PPF parametar value
        Params:
        - param: ppf parameter string to set
        - value: new value string
        """
        file_contents = self.ppf_contents
        found_param = False
        for idx, line in enumerate(file_contents):
            if line.startswith(param):
                file_contents[idx] = '{}\t=\t{}\n'.format(param, value)
                found_param = True
        if not found_param:
            print('[DEBUG] Unhandled situation')
        self.ppf_contents = file_contents
    
    def sanitize_parameter(self, param, illegal_chars, replacement_char):
        """Searches out matching params removes illegal chars and replaces
            them with replacement_char
        Parameters:
        - param: PPF parameter string
        - illegals_chars: string containing chars to remove
        - replacement_char: character that will replace illegal chars
        """
        translator = _make_str_translation(illegal_chars, replacement_char)
        file_contents = self.ppf_contents
        found_param = False
        for idx, line in enumerate(file_contents):
            if line.startswith(param):
                split_line = line.split()
                str_to_sanitize = split_line[PPF_VALUE_INDEX]  # only sanitize param's value   
                sanitized_str = str_to_sanitize.translate(translator)
                if str_to_sanitize != sanitized_str:
                    file_contents[idx] = sanitized_str
                found_param = True
        if not found_param:
            print('[DEBUG] Unhandled situation')
        self.ppf_contents = file_contents
        

    def write(self, filename=''):
        """Writes ppf_contents to file
        Parameters:
        - filename: write contents to another file
        """
        write_filen = filename
        if self.file_overwrite and not write_filen:
            write_filen = self.ppf_file
        elif not self.file_overwrite and not write_filen:
            raise PPFFileError('PPF file is protected from overwriting, \
                must specify filename')
        _write_file_contents(write_filen, self.ppf_contents)


def open_ppf(filep, overwrite=False):
    """Opens a file as ppf file
    """
    ppf_file = PPFFile(filep, overwrite)
    return ppf_file


if __name__ == "__main__":
    # pylint: disable-msg=C0103
    print('Nothing to do')
