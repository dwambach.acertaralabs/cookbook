#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
#sonosite_ppfs.py
"""Modifies PPF files for Sonosite customers
Specifically Looks to change(s) to the following section(s):
 ReportTemplate_BothTests    =    User\\ReportTemplate_BothTests_Acertara_PerElementZ_Header.html
"""
import utils.argparse_utils as argparse_utils  # pylint: disable-msg=E0401
import ppf_tools  # pylint: disable-msg=E0401

PROGRAM_DESC = 'Converts older Sonosite ATLAS parameter files (PPFs)'

SONOSITE_PARAMS = [('ReportTemplate_BothTests',
    'User\\ReportTemplate_BothTests_Acertara_PerElementZ_Header.html')]


def run_tasks_on_file(filep):
    """Will perform tasks on filep
    NOTE: this function is sort of a generic placeholder
    where a different script can be called
    """
    ppf_file = ppf_tools.open_ppf(filep, overwrite=True)
    for (param, value) in SONOSITE_PARAMS: 
        ppf_file.set_parameter(param, value)
    ppf_file.write()


if __name__ == '__main__':
    # pylint: disable-msg=C0103
    filename_parser = argparse_utils.FileNamesArgParser()
    filename_parser.argparser.description = PROGRAM_DESC
    filename_parser.parse_args()
    workon_files = filename_parser.passed_in_files

    if len(workon_files) > 0:
        print('[*] Number of files to read: {}'.format(len(workon_files)))
        err_occured = False
        for workon_file in workon_files:
            try:
                run_tasks_on_file(workon_file)
            except OSError as err:
                err_occured = True
        if err_occured:
            print('[!] Finished with errors.')
        else:
            print('[*] Successfully read {} file(s)'.format(len(workon_files)))
    else:
        print('[!] No files read.')
