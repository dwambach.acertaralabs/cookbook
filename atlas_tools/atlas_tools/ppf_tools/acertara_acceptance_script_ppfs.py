#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
#acertara_acceptance_script_ppfs
"""Modifies PPF files
Looks to change the following section:
<acceptance_script>
Path    =    User\\acceptance_standard_acertara.py
</acceptance_script>
If section doesn't already exist, create it
"""
import utils.argparse_utils as argparse_utils  # pylint: disable-msg=E0401
import ppf_tools  # pylint: disable-msg=E0401

PROGRAM_DESC = 'Converts older ATLAS parameter files (PPFs) acceptance_script'


def replace_acceptance_script(file_contents):
    """Replaces acceptance script value
    """
    # new_l = os.linesep
    desired_lines = ['<acceptance_script>',
                     'Path    =    User\\acceptance_standard_acertara.py',
                     '</acceptance_script>']
    desired_param = desired_lines[1].split()[0]
    in_section = False
    found_param = False
    for idx, line in enumerate(file_contents):
        if line.startswith(desired_lines[0]):
            print('[DEBUG] in section')
            in_section = True
        if in_section and desired_param in line:
            print('[DEBUG] Value found. Changing it')
            file_contents[idx] = '{}\n'.format(desired_lines[1])
            found_param = True
    if not found_param:
        print('[DEBUG] Value not found. Adding it')
        for dline in desired_lines:
            file_contents += ['{}\n'.format(dline)]
    return file_contents


def run_task_on_file(filep):
    """Will perform tasks on filep
    NOTE: this function is sort of a generic placeholder
    where a different script can be called
    """
    # file_contents = file_utils.get_stripped_file_contents(filep)
    ppf_file = ppf_tools.open_ppf(filep, overwrite=True)
    ppf_file.ppf_contents = replace_acceptance_script(ppf_file.ppf_contents)
    ppf_file.write()


if __name__ == '__main__':
    # pylint: disable-msg=C0103
    filename_parser = argparse_utils.FileNamesArgParser()
    filename_parser.argparser.description = PROGRAM_DESC
    filename_parser.parse_args()
    workon_files = filename_parser.passed_in_files

    if len(workon_files) > 0:
        print('[*] Number of files to read: {}'.format(len(workon_files)))
        err_occured = False
        for workon_file in workon_files:
            try:
                run_task_on_file(workon_file)
            except OSError as err:
                err_occured = True
        if err_occured:
            print('[!] Finished with errors.')
        else:
            print('[*] Successfully read {} file(s)'.format(len(workon_files)))
    else:
        print('[!] No files read.')
