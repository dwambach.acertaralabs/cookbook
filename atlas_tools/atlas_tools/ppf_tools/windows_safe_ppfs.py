#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
#windows_safe_ppfs.py
"""Modifies PPF files so that no Windows-illegal filenames are specified 
Specifically Looks to change(s) to the following section(s):
Windows filenames can not contain the following characters: /\"*?|<>
Some Model names saved in PPFs contain some of these bad characters,
which then causes ATLAS to throw an error when the user saves a report

This script will recursively traverse a directory tree looking for PPF
files (.tab file extension, can be set with FILE_EXT) and replace any illegal chars
(can be set via ILLEGAL_CHARS) in the Model
name (can be set with PARAM_STR) with a '-' character (can be changed via REPLACEMENT_CHAR) variable
"""
import utils.argparse_utils as argparse_utils  # pylint: disable-msg=E0401
import ppf_tools  # pylint: disable-msg=E0401

PROGRAM_DESC = 'Converts older Sonosite ATLAS parameter files (PPFs)'

SANITIZE_PARAMS = ['Model']
ILLEGAL_CHARS = ':\\/<>*?|'  # Characters that can not be in a Windows filename
REPLACEMENT_CHAR = '-'  # Replace any and all ILLEGAL_CHARS with REPLACEMENT_CHAR


def run_tasks_on_file(filep):
    """Will perform tasks on filep
    NOTE: this function is sort of a generic placeholder
    where a different script can be called
    """
    ppf_file = ppf_tools.open_ppf(filep, overwrite=True)
    for param in SANITIZE_PARAMS: 
        ppf_file.sanitize_parameter(param, ILLEGAL_CHARS, REPLACEMENT_CHAR)
    ppf_file.write()


if __name__ == '__main__':
    # pylint: disable-msg=C0103
    filename_parser = argparse_utils.FileNamesArgParser()
    filename_parser.argparser.description = PROGRAM_DESC
    filename_parser.parse_args()
    workon_files = filename_parser.passed_in_files

    if len(workon_files) > 0:
        print('[*] Number of files to read: {}'.format(len(workon_files)))
        err_occured = False
        for workon_file in workon_files:
            try:
                run_tasks_on_file(workon_file)
            except OSError as err:
                err_occured = True
        if err_occured:
            print('[!] Finished with errors.')
        else:
            print('[*] Successfully read {} file(s)'.format(len(workon_files)))
    else:
        print('[!] No files read.')
