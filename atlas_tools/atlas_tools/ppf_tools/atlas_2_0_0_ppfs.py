#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
#atlas_2_0_0_ppfs.py
r"""Modifies PPF files for ATLAS v2.0.0 support

ATLAS v2.0.0 PPF changes:

ReportTemplate settings changed, adding User\ dir, and changing _Standard.html to _Default.html
Prior to ALAS v2.0.0:
    ReportTemplate_PulseTest    =   ReportTemplate_PulseTest_Standard.html

ATLAS v2.0.0 and higher require the following format:
    ReportTemplate_PulseTest    =   User\ReportTemplate_PulseTest_Default.html
"""
import utils.argparse_utils as argparse_utils  # pylint: disable-msg=E0401
import ppf_tools  # pylint: disable-msg=E0401

PROGRAM_DESC = 'Converts older ATLAS parameter files (PPFs) to be v2.0.0 compatible'

PARAM_STR = 'ReportTemplate_'
PARAM_VALUE_PREFIX = 'User\\'
INSERT_AFTER_STR = '\t=\t'  # Insert INSERT_STR after this sub_str
SEARCH_REPLACE_STRS = ('Standard', 'Default')  # Search for first str, replace with the second str


def convert_line_to_2_0_0(old_str):
    """given old_str, perform necessary changes before returning new_str"""
    # test if line has already been modified
    # (ie. we dont want to insert INSERT_STR again)
    if old_str.find(PARAM_VALUE_PREFIX) != (-1):
        new_str = old_str
    else:
        insert_idx = old_str.find(INSERT_AFTER_STR) + len(INSERT_AFTER_STR)
        new_str = old_str[:insert_idx] + PARAM_VALUE_PREFIX + old_str[insert_idx:]
        new_str = new_str.replace(SEARCH_REPLACE_STRS[0], SEARCH_REPLACE_STRS[1])
    return new_str

def run_tasks_on_file(filep):
    """Will perform tasks on filep
    NOTE: this function is sort of a generic placeholder
    where a different script can be called
    """
    ppf_file = ppf_tools.open_ppf(filep, overwrite=True)
    for (idx, ppf_value) in enumerate(ppf_file.ppf_contents):
        if ppf_value.startswith(PARAM_STR):
            new_param_value = convert_line_to_2_0_0(ppf_value)
            ppf_file.ppf_contents[idx] = new_param_value
    ppf_file.write()


if __name__ == '__main__':
    # pylint: disable-msg=C0103
    filename_parser = argparse_utils.FileNamesArgParser()
    filename_parser.argparser.description = PROGRAM_DESC
    filename_parser.parse_args()
    workon_files = filename_parser.passed_in_files

    if len(workon_files) > 0:
        print('[*] Number of files to read: {}'.format(len(workon_files)))
        err_occured = False
        for workon_file in workon_files:
            try:
                run_tasks_on_file(workon_file)
            except OSError as err:
                err_occured = True
        if err_occured:
            print('[!] Finished with errors.')
        else:
            print('[*] Successfully read {} file(s)'.format(len(workon_files)))
    else:
        print('[!] No files read.')
