#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
#argparse_utils.py
"""Collection of useful functions dealing with argparsing

Available classes:
- FileNamesArgParser: Argument parsing with basic ways to read in a list of files
    (e.g. recurse directory, read in file, etc)
"""
import argparse
import os

class FileNamesArgParser:
    """Represents an argparse.ArgumentParser() with basic arguments for reading in list of filenames

    Public attributes:
    - argparser: an argparse.ArgumentParser()
    - args: the result of argparse.parse_args()
    - passed_in_files: set of all filenames specified through the argparser
    """
    def __init__(self, argparser=None):
        if argparser:
            if isinstance(argparser, argparse.ArgumentParser):
                self.argparser = argparser
            else:
                raise TypeError('Expected ArgumentParser, got {}'.format(type(argparse)))
        else:
            self.argparser = argparse.ArgumentParser()
        self.argparser.add_argument('-r', dest='recursive', help='Recursive traversing')
        self.argparser.add_argument('-f', dest='read_file',
                                    help='File containing list of files to search')
        self.argparser.add_argument('files', nargs='*', help='File(s) to search')

        self._passed_in_files = None  # will be set in property block below
        self.args = None  # will be set eiher by parse_args() call, or call to workon_files property

    def parse_args(self):
        """Runs parse_args(), sets args"""
        self.args = self.argparser.parse_args()

    @property
    def passed_in_files(self):
        """collect and return all the file names as a set()"""
        if not self._passed_in_files:
            self._passed_in_files = set()  # set of files to be worked on

        if not self.args:
            # the arguments haven't been parsed yet
            self.parse_args()

        if self.args.files:
            for filep in self.args.files:
                self._passed_in_files.add(filep.strip())
        if self.args.read_file:
            with open(self.args.read_file) as freader:
                for filep in freader:
                    self._passed_in_files.add(filep.strip())
        if self.args.recursive:
            for (root, _, files) in os.walk(self.args.recursive):
                for filename in files:
                    filepath = os.path.join(root, filename)
                    self._passed_in_files.add(filepath.strip())
        return self._passed_in_files


if __name__ == '__main__':
    # pylint: disable-msg=C0103
    try:
        filename_parser = FileNamesArgParser()
        filename_parser.argparser.add_argument('-v', dest='verbose',
                                               action='store_true',
                                               help='Turn verbosity on')
        filename_parser.parse_args()
        filenames = filename_parser.passed_in_files
        if filename_parser.args.verbose:
            for filen in filenames:
                print('\t{}'.format(filen))
        print('[*] {} file(s) passed in'.format(len(filenames)))
    except Exception as err:
        raise err
