#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
#datetime_utils.py
"""Collection of useful functions dealing with datetime operations

Available functions:
- get_datetime: returns datetime from string using strptime
- get_t_delta: returns time difference between two datetime objects
"""
from datetime import datetime


def get_datetime(data_str, format_str):
    """
    returns strptime(data_str, format_str)
    """
    ret_str = datetime.strptime(data_str, format_str)
    return ret_str


def get_t_delta(time0, time1):
    """
    returns seconds between datetime obj intervals
    """
    time_delta = time1 - time0
    return time_delta.seconds


if __name__ == '__main__':
    print('Nothing to do')
