#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
#file_utils.py
"""Collection of useful functions dealing with file operations

Available functions:
- get_stripped_file_contents: Opens and reads a file's contens,
    returns the contents stripped of white space
"""

def get_stripped_file_contents(filep):
    """Read in contents of filep return contents with whitespace removed"""
    file_contents = []
    with open(filep) as freader:
        for line in freader:
            file_contents.append(line.strip())
    return file_contents


if __name__ == '__main__':
    print('Nothing to do')
