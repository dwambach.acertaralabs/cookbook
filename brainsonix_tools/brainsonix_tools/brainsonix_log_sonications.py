#!/usr/bin/env python3
"""brainsonix_log_sonications.py
This script is meant to parse a Brainsonix System log file,
and search for treatment 'runs' and count the number of sonications

This is in response to issue-19, where it appears that
sometimes a pause between sonications does not happen
(i.e a sonication will run straight into another one)

Would like a way to quickly parse many log files to see
how far reaching this issue is

For usage instructions:
> brainsonix_log_sonications.py -h
"""
import os

import utils.argparse_utils as argparse_utils  # pylint: disable-msg=E0401
import utils.datetime_utils as datetime_utils  # pylint: disable-msg=E0401
import utils.file_utils as file_utils  # pylint: disable-msg=E0401

PROGRAM_DESC = 'Parses brainsonix log files; counts number of sonications per treatment run'

BSX_LOG_FORMAT = '%Y%m%dT%H%M%S.%f'  # for datetime str formatting

TREATMENT_START_DELIM = 'Treatment Initiated'
TREATMENT_END_DELIM = 'Treatment Complete'

SONICATION_DURATION_DELIM = 'Sonication Duration'
"""The line with sonication duration opts, will appear as follows:
Sonication Duration(s):30.0000  Time Between Sonications(s):30.0000 Sonication Number: 10
"""

SONICATION_START_DELIM = 'Sonication Started'
SONICATION_END_DELIM = 'Sonication Completed'
SONICATION_NUMBER_DELIM = 'Sonication Number:'
SONICATION_METADATA_LINE_COUNT = 10
SONICATION_METADATA_PRINTOUT_IDXS = [3, 6]

HTML_DOC_FORMAT = '''<!DOCTYPE html>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Brainsonix Log Report</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <style type="text/css">
            .body {{
                height: auto;
            }}

            .collapsible {{
              width: 90%;
              margin-left: 5%;
              cursor: pointer;
              padding: 18px;
              text-align: left;
              outline: none;
              font-size: 15px;
              overflow: auto;
            }}

            .collapsible-item {{
              width: 100%;
              cursor: pointer;
              padding: 4px;
              text-align: left;
            }}

            .collapsible:after, .collapsible-item:after {{
              content: '\\002B';
              font-weight: bold;
              float: right;
              margin-left: 5px;
            }}

            .active:after {{
              content: "\\2212";
            }}

            .content {{
              width: 90%;
              margin-left: 5%;
              padding: 0 18px;
              max-height: 0;
              overflow: hidden;
              overflow-y: scroll;
              transition: max-height 0.2s ease-out;
              background-color: #f1f1f1;
            }}
            .content-item {{
              padding: 0 18px;
              max-height: 0;
              overflow: hidden;
              overflow-y: scroll;
              transition: max-height 0.2s ease-out;
              background-color: #f1f1f1;
            }}
        </style>
    </head>
    <body>
            {body}
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <script>
            var coll = document.getElementsByClassName("collapsible");
            var coll2 = document.getElementsByClassName("collapsible-item");
            var i;

            for (i = 0; i < coll.length; i++) {{
              coll[i].addEventListener("click", function() {{
                this.classList.toggle("active");
                var content = this.nextElementSibling;
                if (content.style.maxHeight){{
                  content.style.maxHeight = null;
                }} else {{
                  content.style.maxHeight = content.scrollHeight + "px";
                }} 
              }});
            }}
            for (i = 0; i < coll2.length; i++) {{
              coll2[i].addEventListener("click", function() {{
                this.classList.toggle("active");
                var content = this.nextElementSibling;
                if (content.style.maxHeight){{
                  content.style.maxHeight = null;
                }} else {{
                  content.style.maxHeight = "500" + "px";
                }} 
              }});
            }}
        </script>
    </body>
</html>
'''


def brainsonix_parse_all_treatments(contents):
    """Given an array of brainsonix log content
    Return array of arrays where individual treatment runs
    have been parsed out
    """
    treatment_runs = []
    current_run = []
    parsing_treatment = False
    for line_val in contents:
        if TREATMENT_START_DELIM in line_val:
            if not parsing_treatment:
                parsing_treatment = True
            ### Commented out because we're not handling this case right now, but should
            # else:
            #     print('[!] line no. {}: Reached 2nd start delim without reaching an end?'.format(
            #            line_no))
            if current_run:
                treatment_runs.append(current_run)  # possible bad run; append and start a new one
                # keep parsing_treatment at True
            current_run = []
        elif TREATMENT_END_DELIM in line_val:
            if current_run:
                treatment_runs.append(current_run)
            ### Commented out because we're not handling this case right now, but should
            # else:
            #     no need to append an empty run
            #     print('[!] line no. {}: Current run has no elements?'.format(line_no))
            # if not parsing_treatment:
            #     print('[!] line no. {}: Reached end delim without first reaching start?'.format(
            #           line_no))
            current_run = []
            parsing_treatment = False
        else:
            current_run.append(line_val)
    return treatment_runs


def timehack_from_sonication_line(line):
    """
    Log entry: 20200312T154222.974,Sonication Started
    """
    time_str = line.split(',')[0]
    return time_str


def brainsonix_parse_sonications(treatment_run):
    """Given an array containing an individual treatment run
    return array of each sonications
    """
    treatment_run_report = {}
    expected_sonications = 0
    started_sonications = 0
    completed_sonications = 0
    metadata_idx = 0
    metadata = []
    timehack0 = 0
    timehack1 = 0
    numb_delim_len = len(SONICATION_NUMBER_DELIM)
    treatment_run_report['sonications'] = {}
    for line in treatment_run:
        if metadata_idx < SONICATION_METADATA_LINE_COUNT:
            if metadata_idx in SONICATION_METADATA_PRINTOUT_IDXS:
                metadata.append(line)
            metadata_idx += 1
        if SONICATION_NUMBER_DELIM in line:
            # extract the expected number of sonications
            numb_idx = line.index(SONICATION_NUMBER_DELIM) + numb_delim_len
            expected_sonications_str = line[numb_idx:].split()[0]  # just in case
            expected_sonications = int(expected_sonications_str)
            treatment_run_report['expected_sonications'] = expected_sonications
        elif SONICATION_START_DELIM in line:
            timehack0 = timehack_from_sonication_line(line)
            #print('{n}: {s}'.format(n=started_sonications, s=line))
            started_sonications += 1
        elif SONICATION_END_DELIM in line:
            #print('{n}: {s}'.format(n=completed_sonications, s=line))
            timehack1 = timehack_from_sonication_line(line)
            treatment_run_report['sonications'].update(
                {completed_sonications:(timehack0, timehack1)})
            completed_sonications += 1
    treatment_run_report['metadata'] = metadata
    treatment_run_report['started_sonications'] = started_sonications
    treatment_run_report['completed_sonications'] = completed_sonications
    if expected_sonications == started_sonications and \
       expected_sonications == completed_sonications and \
       started_sonications == completed_sonications:
        run_summary = 'OK'
    elif expected_sonications < started_sonications or \
         expected_sonications < completed_sonications:
        run_summary = 'Error detected(ISSUE-19)'
    else:
        run_summary = 'Error detected(UNK)'
    treatment_run_report['summary'] = run_summary
    return treatment_run_report


def brainsonix_log_sonications(bsx_filep):
    """Searches bsx_filep for treatment iterations
    Compares desired sonication iterations with the actual count
    - bsx_filep filename/filepath to a brainsonix log file
    """
    bsx_log_file_report = {}
    try:
        file_contents = file_utils.get_stripped_file_contents(bsx_filep)
    except OSError as err:
        bsx_log_file_report['err_msg'] = repr(err)
    else:
        treatment_runs = brainsonix_parse_all_treatments(file_contents)
        bsx_log_file_report['treatment_runs'] = {}
        bsx_log_file_report['treatment_runs']['count'] = len(treatment_runs)
        bsx_log_file_report['treatment_runs']['runs'] = {}
        if treatment_runs:
            for run_no, run in enumerate(treatment_runs, 1):
                single_run_report = brainsonix_parse_sonications(run)
                bsx_log_file_report['treatment_runs']['runs'].update({'run{}'.format(
                    run_no): single_run_report})
    return bsx_log_file_report


def build_html_report(data):
    """
    data - Dictionary item
    Returns string representing html document
    """
    html_document = '' # string to be returned to caller
    html_body = '<h1 class="display-3"><center>Brainsonix Log Report</center></h1>\n'
    html_report = ''
    for file_report in data.values():
        # print(file_report)
        html_report += '<button type="button" class="collapsible  d-flex justify-content-between \
        align-items-center btn btn-dark">'
        html_report += '<span>{}</span><span class="badge badge-primary badge-pill"> \
        {} run(s)</span>'.format(
            file_report['filename'],
            file_report['report']['treatment_runs']['count'])
        html_report += '</button>\n'
        html_report += '<div class="content list-group">\n'
        html_report += '<li class="list-group-item"><i>Log file location:  {}</i></li>\n'.format(
            file_report['location'])
        
        html_report += '<table class="table">\n'
        for run_numb in file_report['report']['treatment_runs']['runs']:
            html_report += '<tr>\n'
            result_summary = file_report['report']['treatment_runs']['runs'][run_numb]['summary']
            if 'err' in result_summary.lower():
                html_report += '<td class="table-danger"><b>{}: </b>{}</td>\n'.format(run_numb[0].upper() + run_numb[1:], result_summary)
            else:
                html_report += '<td class="table-success"><b>{}: </b>{}</td>\n'.format(run_numb[0].upper() + run_numb[1:], result_summary)
            html_report += '<td>\n'
            html_report += '<table class="table">'
            metadata_strs = file_report['report']['treatment_runs']['runs'][run_numb]['metadata']
            for metadata_str in metadata_strs:
                metadata_str = metadata_to_html(metadata_str)
                html_report += '<tr><td>{}</td></tr>\n'.format(metadata_str)
            started_sonications = file_report['report']['treatment_runs']['runs'] \
                                             [run_numb]['started_sonications']
            completed_sonications = file_report['report']['treatment_runs']['runs'] \
                                               [run_numb]['completed_sonications']
            html_report += '<tr><td>\n'
            html_report += 'Sonication Stats: Started(<b>{}</b>), '.format(started_sonications)
            html_report += 'Completed(<b>{}</b>)</li>\n'.format(completed_sonications)
            html_report += '</tr></td>\n'
            html_report += '<tr><td>\n'
            html_report += '<button type="button" class="collapsible-item btn btn-info">'
            html_report += 'Sonications time table</button>\n'
            sonications_table = sonications_to_html(file_report['report']['treatment_runs'] \
                                                               ['runs'][run_numb]['sonications'])
            html_report += '<div class="content-item">{}</div>\n'.format(sonications_table)
            html_report += '</td></tr>'
            html_report += '</table>'
            html_report += '</td>\n'
            html_report += '</tr>\n'
        html_report += '</table>\n'
        html_report += '</div>\n'  # bottom of class content
        html_report += '<br>\n'
    html_body += html_report
    html_document = HTML_DOC_FORMAT.format(body=html_body)
    return html_document


def sonications_to_html(sonications_data):
    """
    {'sonications': {0: (190434.462, 190504.51), 1: (190534.461, 190604.536)}}
    """
    sonications_html = '<table class="table table-sm table-hover table-striped"><tr> \
    <thead class="thead-dark"><th>#</th><th>Start</th>'
    sonications_html += '<th>End</th><th>Delta</th><th>Pause Duration</th></tr></thead>\n'
    # print(sonications_data)
    t_last_end = None
    for sonication_idx in sonications_data:
        t_start = datetime_utils.get_datetime(sonications_data[sonication_idx][0], BSX_LOG_FORMAT)
        t_end = datetime_utils.get_datetime(sonications_data[sonication_idx][1], BSX_LOG_FORMAT)
        if not t_last_end:
            t_last_end = t_start
        t_pause = datetime_utils.get_t_delta(t_last_end, t_start)

        t_delta = datetime_utils.get_t_delta(t_start, t_end)

        t_last_end = t_end

        if (t_delta == 0 or t_pause == 0) and sonication_idx > 0:
            sonications_html_class = 'table-danger'
        else:
            sonications_html_class = ''
        sonications_html += '<tr class="{}"><td>{}</td><td>{}</td><td>{}</td>'.format(
            sonications_html_class, sonication_idx, t_start, t_end)
        sonications_html += '<td>{}</td><td>{}</td></tr>'.format(
            t_delta, t_pause)
    sonications_html += '</table>\n'
    return sonications_html


def metadata_to_html(metadata_str):
    """
    Pulls out some metadata contained in a treatment report
    'metadata': ['Sonication File:30sON30sOFF_80mm_1000Hz_70DC.bsx', 'Sonication Duration(s):30.00 \
                 Time Between Sonications(s):30.0000,Sonication Number: 10']
    """
    parsed_str = ''
    try:
        if 'Sonication File:' in metadata_str:
            # add space, bold brackets`
            split_str = metadata_str.split(':')
            parsed_str = split_str[0] + ': ' + '<b>{}</b>'.format(split_str[1])
        elif 'Sonication Duration' in metadata_str:
            split_str = metadata_str.split(',')
            for a_str in split_str:
                split_str2 = a_str.split(':')
                parsed_str += split_str2[0] + ': ' + '<b>{}</b> '.format(split_str2[1])
        else:
            parsed_str = 'Unknown Data'
    except IndexError:
        parsed_str = 'Unknown Data'
    return parsed_str


if __name__ == '__main__':
    # parse arguments for files to work on
    # pylint: disable-msg=C0103
    report_data = {}
    # workon_files = argparse_utils.argparse_workon_files(PROGRAM_DESC)
    filename_parser = argparse_utils.FileNamesArgParser()
    filename_parser.argparser.description = PROGRAM_DESC
    filename_parser.parse_args()
    workon_files = filename_parser.passed_in_files
    if len(workon_files) > 0:
        #print('[*] Number of files to read: {}'.format(len(workon_files)))
        for idx, filep in enumerate(workon_files):
            try:
                log_file_report = brainsonix_log_sonications(filep)
            except Exception as err:
                print('[!] file: {}'.format(filep))
                print(repr(err))
            else:
                filename = os.path.basename(filep)
                file_location = os.path.dirname(filep)
                log_report_entry = {'filename':filename,
                                    'location': file_location,
                                    'report': log_file_report}
                report_data[idx] = log_report_entry
        # print ('[*] Done parsing files.')
        #print(repr(report_data))
        #print(json.dumps(report_data, indent=2))
        html_doc = build_html_report(report_data)
        print(html_doc)
